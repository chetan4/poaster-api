module CarrierWave
  module MagickHelper
    extend ActiveSupport::Concern
     
    module ClassMethods
      def resize_to_geometry(width,height)
        process :resize_to_geometry => [width, height]
      end
    end
    
    def resize_to_geometry(new_width,new_height)
      manipulate! do |img|
        new_img = img.resize!(new_width, new_height)
        new_img = yield(new_img) if block_given?
        new_img
      end
    end
  end
end