class YoutubeUrl
  attr_reader :url,:uri
  def initialize(url)
    @url = url
    @query = parse.query
  end

  def parse
    @uri ||= URI.parse(url || '')
  end
    
  def video_id
    pathname || query_string
  end

  def pathname
    shortner? ? uri.path.sub(/\//,'') : nil
  end
  
  def query_string
    @query.split("&").find { |query_param| query_param =~ /v=(.+)/}.split("v=")[1] rescue ""
  end


  def shortner?
    uri.host == 'youtu.be'
  end

  def as_json
    {
      ## Note we are returning a slighly bigger Image for youtube in mobile app then in command poast
      thumb: "http://img.youtube.com/vi/#{video_id}/0.jpg",
      url: "http://www.youtube.com/embed/#{video_id}?autoplay=1"
    }
  end

  def thumb_url
    "http://img.youtube.com/vi/#{video_id}/0.jpg"
  end
end
YoutubeURL = YoutubeUrl


