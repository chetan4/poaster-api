class String
  def join(separator='')
    [self,''].join(separator).strip
  end
end
