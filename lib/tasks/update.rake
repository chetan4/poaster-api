namespace :update do
  desc 'Update the last updated at time'
  task :last_updated_at => :environment do
    puts 'Updating Twitter tweet last_updated_at ..'
    TwitterStatistic.scoped.each do |twitter|
      twitter.last_updated_at.presence || twitter.update_attribute('last_updated_at',DateTime.now)
    end
    
    puts ''
    
    puts 'Updating Facebook post last_updated_at ..'
    FacebookStatistic.scoped.each do |facebook|
      facebook.last_updated_at.presence || facebook.update_attribute('last_updated_at',DateTime.now)
    end
    
    puts ''
    
    puts 'Updating Linkedin post last_updated_at ..'
    LinkedinStatistic.scoped.each do |linkedin|
      linkedin.last_updated_at.presence || linkedin.update_attribute('last_updated_at',DateTime.now)
    end
    
    puts ''
    
  end
end  
