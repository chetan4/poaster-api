namespace :measurable do
  task :twitter => :environment do
    Measurable.provider=:twitter
    Measurable.start
  end
  
  task :facebook => :environment do
    Measurable.provider=:facebook
    Measurable.start 
  end
  
  task :linkedin => :environment do
    Measurable.provider=:linkedin
    Measurable.start
  end 
end
