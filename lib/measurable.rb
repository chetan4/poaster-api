class ConfigurationError < StandardError
  def initialize(message)
    super(message)
  end  
end  
module Measurable
  extend self
  ## Since facebook allow 600 call for user currently look like it inclusive of post as well 
  mattr_accessor :provider

  def start
    configuration!
    Authorization.where(provider: provider).each do |credential|
      #Thread.new { 
      #  ::Measurable::Poaster.start(user_id: credential.user_id,provider: provider) 
      #}
      ::Measurable::Poaster.start(user_id: credential.user_id,provider: provider) 
    end   
  end

  def configuration!
    provider.nil? or raise ConfigurationError.new('Please attach a provider valid provider are :twitter,:facebook,:linkedin')
  end  
end

### USAGE need to wheneverize this on cron for 15 min
#Measurable.provider=:twitter
#Measurable.start
