class SocialProvider::Email < SocialProvider::SocialShare
  attr_reader :name,:to,:from,:cc,:bcc,:provider,:subject,:body,:tracker,:response
  def initialize(options={})
    @provider = 'email'
    @name = options[:name]
    @to = options[:to]
    @from = options[:from]
    @cc = options[:cc]
    @bcc = options[:bcc]
    @subject = options[:subject]
    @body = options[:body]
    @tracker = EmailStatistic
    super(options)
  end

  def share
    PoasterMailer.share(wall_poast).deliver
    @response = {id: share_id}
    yield self if block_given?
    rescue  => exception
      fail!(exception)
  end

  def as_json
    wall_poast
  end

  private

  def share_id
    @share_id ||= SecureRandom.hex(16)
  end

  def default_subject
    [name,'shared',shareative.name,'with you'].join(' ')
  end

  def default_body
    shareative_lookup
  end

  def signature
    'Poaster Team <br/>
    info@poaster.me'
  end

  def wall_poast
    {
      from: from,
      to: to,
      cc: cc,
      bcc: bcc,
      subject: subject || default_subject,
      body: body || default_body,
      share_id: share_id
    }
  end
end  