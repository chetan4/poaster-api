class SocialProvider::Twitter < SocialProvider::SocialShare
  attr_reader :provider,:tracker,:response
  def initialize(options)
    @provider = 'twitter'
    @tracker = TwitterStatistic
    super(options)
  end
  
  def share
    @response = twitter.update(wall_poast)
    yield self if block_given?
    rescue ::Twitter::Error::Unauthorized  => exception
      # deauthorize the user access token
      deauthorize!
      fail!(exception)
    rescue ::Twitter::Error => exception
      fail!(exception)
  end
  
  private
  def reset_access_token
    ## Currently we do reset access token stuff
    true
  end

  def wall_poast
    [shareative_lookup,message].join(" ").truncate(140)
  end

  def twitter
    ::Twitter::REST::Client.new do |config|
      config.consumer_key        = ApiConfig.twitter_app_id
      config.consumer_secret     = ApiConfig.twitter_app_secret
      config.access_token        = access_token
      config.access_token_secret = secret_token
    end
  end   
end