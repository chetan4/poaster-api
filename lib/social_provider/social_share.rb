require 'action_controller/metal'
class SocialProvider::SocialShare
 include Rails.application.routes.url_helpers
  attr_reader :shareative_id,:message,:user_id,:company_id
  class << self
    def share(content,&block)
      new(content).share(&block)
    end
  end

  def initialize(options)
    @shareative_id = options[:shareative_id]
    @message = options[:message]
    @user_id = options[:user_id]
    @company_id = options[:company_id]
  end  

  def access_token
    find_access_token
  end
  
  def secret_token
    find_secret_token
  end

  def shareative
    Shareative.find_by(id: @shareative_id)
  end
    
  def share
    raise "Attempt to call this method will raise an error"
  end

  def track
    tracker.create(tracking_options)
    return {status: :success,provider: provider}
    ## Do this via callback that is better
    # tracker.update_statistics
  end

  private

  def fail!(exception)
    {status: :failed,reason: exception.message,provider: provider}
  end

  def slug_name
    shareative.slug_name
  end

  def deauthorize!
    Authorization.find_by(provider: provider,user_id: user_id).destroy rescue true
  end

  def tracking_options
    {
      share_id: response[:id] || response['id'],
      user_id: user_id,
      shareative_id: shareative.id.to_s,
      company_id: company_id
    }
  end

  def find_access_token
    authorization = Authorization.find_by(provider: provider,user_id: user_id)
    return authorization.oauth_token if authorization.oauth_token_valid?
    #reset_access_token
  end

  def find_secret_token
    authorization = Authorization.find_by(provider: provider,user_id: user_id)
    authorization.oauth_secret
  end

  def shareative_lookup
    show_poaster_url(shareative,name: slug_name)
  end  
end