class SocialProvider::Linkedin < SocialProvider::SocialShare
  attr_reader :provider,:tracker,:response
  def initialize(options)
    @provider = 'linkedin'
    @tracker = LinkedinStatistic
    super(options)
  end
  

  def share
    response = linkedin.add_share(wall_poast)
    unless response.code.to_i == 201
      raise LinkedIn::Errors::LinkedInError.new("linkedin responded with error code #{response.code}, Please report to us")
    end
    ## if the status code is somehow does not match 201 raise an error
    @response = {id: parse(response.body)}
    yield self if block_given?
    rescue LinkedIn::Errors::UnauthorizedError => exception
      deauthorize!
      fail!(exception)
    rescue LinkedIn::Errors::LinkedInError => exception
      fail!(exception)
  end

  private
  def reset_access_token
    ## no reset mechanism currently
  end

  def linkedin
    client = LinkedIn::Client.new
    client.authorize_from_access(access_token,secret_token)
    client
  end  

  def wall_poast
    { 
      comment: message,
      content: {
        title: shareative.name,
        'submitted-image-url' => shareative.thumb_url,
        'submitted-url' =>  shareative_lookup,
        description: message,
      },
      visibility: {
        code: 'anyone'
      }
    }
  end

  def parse(body)
    JSON.parse(body)['updateKey']
  end  
end  