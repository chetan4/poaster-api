class SocialProvider::Facebook < SocialProvider::SocialShare
  attr_reader :provider,:tracker,:response
  def initialize(options)
    @provider = 'facebook'
    @tracker = FacebookStatistic
    super(options)
  end


  def share
    handler = Koala::Facebook::API.new(access_token)
    @response = handler.put_connections('me','feed',wall_poast)
    yield self if block_given?
    rescue Koala::Facebook::ClientError => exception
      revoked!(exception) { deauthorize! }
      fail!(exception)
    rescue Koala::Facebook::AuthenticationError => exception
      deauthorize!
      fail!(exception)
    rescue Koala::Facebook::OAuthSignatureError => exception
      deauthorize!
      fail!(exception)
    rescue Koala::Facebook::OAuthTokenRequestError => exception
      deauthorize!
      fail!(exception)
    rescue Koala::Facebook::API => exception
      ## Catch generic exception highly unlikely this is ever be called but failsafe condition
      fail!(exception)
    rescue Koala::Facebook::ServerError => exception
      fail!(exception)
  end

  private
  def reset_access_token
    ## currently we dont do anything for reset access token
    true
  end

  def wall_poast
    {
      message: message,
      link: shareative_lookup,
      name: shareative.name,
      caption: shareative.name,
      description: shareative.description,
      source: shareative.thumb_url
    }
  end

  def revoked!(exception)
    yield if _revoked?(exception) and block_given?
  end

  def _revoked?(exception)
    valid_error_type?(exception.fb_error_type) and (valid_error_code?(exception.fb_error_code) or valid_error_subcode?(exception.fb_error_subcode))
  end

  def valid_error_type?(error_type='')
    error_type == 'OAuthException'
  end

  def valid_error_code?(error_code)
    error_code == 10 or error_code == 200
  end

  def valid_error_subcode?(error_subcode)
    error_subcode == 458 or error_subcode == 463 or error_subcode == 464 or error_subcode == 467 or error_subcode == 460
  end  
end  