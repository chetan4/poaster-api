module UsersStatistic
  extend ActiveSupport::Concern
  included do
    belongs_to :shareative
    validates_presence_of :user_id,:shareative_id
    before_create :last_updated_at_time
    after_create :update_statistic
    scope :statistics,->(id) { where(company_id: id) }
  end
  

  private
  def last_updated_at_time
    self.last_updated_at = DateTime.now
  end
   
  def update_statistic
    Rails.logger.info '...Tracking...'
    Rails.logger.info '...Tracking...'
    Rails.logger.info '...Tracking...'
    Rails.logger.info '...Tracking...'
    statistic = Statistic.find_or_create_by(user_id: user_id,company_id: company_id)
    statistic.inc(column,1)
    statistic.inc(:total_share_count,1)
  end
  #end  
end  
