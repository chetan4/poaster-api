require 'parallel'
class Measurable::Poaster
  attr_reader :user_id,:objects,:provider,:threads,:klass,:client
  
  def self.start(options)
    "Measurable::#{options[:provider].capitalize}".constantize.new(options).start
  end
  
  def initialize(options)
    @provider = options.fetch(:provider)
    @user_id = options.fetch(:user_id)
    @threads = 2
    client!
  end
  
  def start
    _any? and find_limit_remaining and find_statistics and query
  end
 
    
  protected
  
  def info(message)
    puts "[INFO] #{message} for user_id : #{user_id} at #{Time.now}".white.bold
  end
  
  def error(message)
    puts "[ERROR] #{message} for user_id : #{user_id} at #{Time.now}".red.bold
  end

  def _any?
    info('Inside any?')
    klass.where(user_id: user_id).count > 0
  end
  
  def access_token
    info('Inside access_token')
    @access_token ||= Authorization.find_by(user_id: user_id,provider: provider).oauth_token
  end
  
  def secret_token
    info('Inside secret_token')
    @secret_token ||= Authorization.find_by(user_id: user_id,provider: provider).oauth_secret
  end
 
  def find_statistics
    info('Inside find_statistics')
    @objects = klass.where(user_id: user_id).asc(:last_updated_at).limit(remaining)  
  end
  
  def query
    info('Inside query')
    ## Fire no max then then 10 thread at a time
    #Parallel.map(objects,:in_threads => threads) do |object|
    objects.each do |object| 
      find_measurable_and_store!(object)
    end
  end
  
  def find_measurable_and_store!(object)
    info('Inside find_measurable_and_store!')
    store!(object)
  end
  
  def store!(object)
    info('Inside store!')
    object.update_attributes find_measurable(object)
  end
end
