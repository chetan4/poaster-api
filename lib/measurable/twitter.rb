class Measurable::Twitter < Measurable::Poaster
  attr_accessor :limit,:remaining,:reset
  
  def initialize(options)
    @klass = TwitterStatistic
    super
  end
  
  private
  def find_measurable(object)
    info("Inside find_measurable #{object.share_id}")
    {retweets: find_retweet(object) ,last_updated_at: Time.now}
  end
  
  def find_retweet(object)
    info("Inside find_retweet of #{object.share_id}")
    client.status(object.share_id).retweet_count rescue 0
  end

  def find_limit_remaining
    info('Inside find_limit_remaining')
    client.rate_limit_status(resources: 'statuses').tap do |object|
      set_object_accessors(object)
    end 
    rescue Exception => e
      error("Exception #{e.message}")
      false
  end
  
  def client!
    @client = begin
      ::Twitter::REST::Client.new do |config|
        config.consumer_key        = ApiConfig.twitter_app_id
        config.consumer_secret     = ApiConfig.twitter_app_secret
        config.access_token        = access_token
        config.access_token_secret = secret_token
      end
    end  
  end
  
   
  def set_object_accessors(object)
    info('Inside set_object_accessors')
    object.attrs.fetch(:resources).fetch(:statuses).fetch(show).each do |i,j|
      send("#{i}=",j)
    end
    info("Limit: - #{limit}")
    info("Remaining :- #{remaining}")
    info("Reset :- #{reset}")
  end
  
  def show
    :"/statuses/show/:id"
  end
end
