class Measurable::Facebook < Measurable::Poaster
  attr_accessor :limit,:remaining,:reset
  def initialize(options)
    super
    @klass = FacebookStatistic
    @limit = 300
    @reset = 10.minutes.from_now
  end
 
  private
  def find_limit_remaining
    info('Inside find_limit_remaining')
    @remaining = 300 
  end
  
  def find_measurable(object)
    info("Inside find_measurable of #{object.share_id}")
    { likes: client.get_object("#{object.share_id}/likes?summary=1").count, comments: client.get_object("#{object.share_id}/comments?summary=1").count, last_updated_at: Time.now}
  end
  
  def client!
    @client = Koala::Facebook::API.new(access_token)
  end 
end  
