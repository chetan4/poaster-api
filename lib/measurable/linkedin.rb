class Measurable::Linkedin < Measurable::Poaster
  attr_accessor :limit,:remaining,:reset
  def initialize(options)
    super
    @klass = LinkedinStatistic
    @limit = 300
    @reset = 1.day.from_now.midnight
  end
    
  private
  
  def find_limit_remaining
    @remaining = 300
  end
end

