module Twitter
  module RateLimitStatus
    include Twitter::REST::Utils
    include Twitter::Utils
    def rate_limit_status(options={})
      perform_with_object(:get, "/1.1/application/rate_limit_status.json",options,Twitter::RateLimit)
    end
  end

  module REST
    class Client
      include RateLimitStatus
    end
  end    
end
