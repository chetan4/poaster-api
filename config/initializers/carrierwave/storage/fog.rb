CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => ApiConfig.s3_provider,
    :aws_access_key_id      => ApiConfig.aws_access_key_id,
    :aws_secret_access_key  => ApiConfig.aws_secret_access_key
  }
  config.fog_directory  = ApiConfig.directory
  config.fog_public     = ApiConfig.make_public
end
