LinkedIn.configure do |config|
  config.token = ApiConfig.linkedin_app_id
  config.secret = ApiConfig.linkedin_app_secret
end