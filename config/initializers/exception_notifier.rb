## Exception notifier
Rails.application.config.middleware.use ExceptionNotifier,
:email_prefix => "[#{Rails.env}] Error Occurred ",
:sender_address => ApiConfig.sender_email_address,
:exception_recipients => ApiConfig.receiver_email_address