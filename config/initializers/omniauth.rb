

OmniAuth.configure do |omni_auth|
  omni_auth.full_host = ApiConfig.host
  #omni_auth.on_failure = Proc.new { |env| OmniAuth::FailureEndpoint.new(env).redirect_to_failure }
end


Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook,ApiConfig.facebook_app_id,ApiConfig.facebook_app_secret,auth_type: 'reauthenticate',scope: 'publish_actions,user_activities,user_likes,email',display: 'touch', provider_ignores_state: true
  provider :twitter,ApiConfig.twitter_app_id, ApiConfig.twitter_app_secret
  provider :linkedin,ApiConfig.linkedin_app_id, ApiConfig.linkedin_app_secret ,:scope =>"r_basicprofile,rw_nus"
  ## We are not using Google+ for now
  # provider :google_oauth2, '905558974341-e5gsk3cai3lar88d1agk8rmk5ej4qkud.apps.googleusercontent.com', 'MAVgjnf7kxos2kWoNsSztlth'
  #   {
  #     :name => "google",
  #     :scope => "userinfo.email,userinfo.profile,plus.me"
  #     :prompt => "select_account",
  #     :redirect_uri => 'http://localhost:4005/auth/google_oauth2/callback',
  #     :access_type: 'offline',
  #     :include_granted_scopes => true
  #   }
end
Rails.application.config.middleware.insert_before OmniAuth::Builder,OmniAuthMiddleware

