set :application, 'poaster_api'
set :branch, 'master'
set :scm, :git
set :ssh_options, { :forward_agent => true }
set :format, :pretty
set :log_level, :debug
set :rvm_type, :user   # Defaults to: :auto
set :rvm_ruby_version, 'ruby-1.9.3-p484@poaster_api'
#set :rvm_custom_path, '~/.myveryownrvm'  # only needed if not detected
# set :pty, true
set :linked_files, %w{config/mongoid.yml config/api_config.yml config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}

#set :default_env, { path: "$HOME/rvm/bin/rvm/ruby-1.9.3-p484/bin:$PATH" }
set :keep_releases, 5
#SSHKit.config.command_map[:cp_r]  = "cp -r"
before "deploy:symlink:shared","deploy:copy_files"

namespace :deploy do
  desc 'Copy files from application to shared directory'
  task :copy_files do
    on roles(:app) do
      execute :cp,'-f',release_path.join('support/config/api_config.yml'),shared_path.join('config')
    end  
  end
  
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do 
      # Your restart mechanism here, for example:
      puts ".......Running Restart stuff please do this......."
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc 'Reset Log file'
  task :log_clear do 
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      puts '.....Clearing the log file ...'
      execute "cd #{release_path}; bundle exec rake log:clear"
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
  after :finishing, 'deploy:cleanup'
  after 'deploy:publishing', 'deploy:restart','deploy:log_clear'
end
