# Load the rails application
require File.expand_path('../application', __FILE__)
require 'core_extensions'
require 'carrierwave/magick_helper'
# Initialize the rails application
PoasterApi::Application.initialize!
