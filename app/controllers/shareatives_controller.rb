class ShareativesController < ApplicationController
  skip_before_filter :authenticate_user! , only: [:show]
  def show
    @shareative = Shareative.find(params[:id])
    render layout: 'shareative'
  end

  def email_content
    email_share = SocialProvider::Email.new(shareative_id: params[:id],from: current_user.email,name: current_user.full_name)
    render json: (email_share.as_json rescue {})
  end
end