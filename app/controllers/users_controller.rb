class UsersController < ApplicationController
  def show
    respond_to do |format|
      format.json { render json: current_user.as_json }
    end
  end

  def new_password ; end

  def create_password
    if params[:password_options].present? and create_password?
      flash[:success] = "Successfully updated password"
      render json: {status: "success", message: 'Password has been updated'}
    else
      render json: {status: 'failed', message: humaize_errors }
    end
  end


  def statistics
    render json: current_user.statistics
  end

  def profile_picture
    delete_unwanted_params
    if current_user.update_attributes(params[:user])
      render json: { status: :success,small_pic: current_user.profile.url(:thirty), big_pic: current_user.profile.url(:forty_two) }
    else
      render json: { status: :failed,small_pic: current_user.profile.url(:thirty), big_pic: current_user.profile.url(:forty_two),message: humaize_errors}
    end
    rescue => exception
      render json: { status: :failed, small_pic: current_user.profile.url(:thirty), big_pic: current_user.profile.url(:forty_two),message: exception.message}
  end  

  private
  def create_password?
    current_user.validate_password?(params[:password_options].delete(:old_password)) and current_user.update_attributes(params[:password_options])
    current_user.errors.empty?
  end 

  def humaize_errors
    current_user.errors.values.flatten.join(" ")
  end


  def delete_unwanted_params
    params[:user].reject! { |key,value| key.to_sym != :profile }
  end
end
