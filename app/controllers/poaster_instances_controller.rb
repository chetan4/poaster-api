class PoasterInstancesController < ApplicationController
  respond_to :json
  # poaster_instances.json
  def index
    ## Return all Poaster Instances of the given user
    @poaster_instances = current_user.poaster_intances
    respond_with @poaster_instances.as_json
  end
  # /poaster_intances/[:id].json

  def show
    ## Find Poaster Instance and return it json along with facade and shareatives
    @poaster_instance = current_user.build_poaster_json(params[:id])
    respond_with @poaster_instance
  end
end
