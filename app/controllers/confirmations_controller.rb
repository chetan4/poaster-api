class ConfirmationsController < Devise::ConfirmationsController
  def show
    confirmation_token = Devise.token_generator.digest(resource_class, :confirmation_token, params[:confirmation_token])
    self.resource = resource_class.find_by_confirmation_token(confirmation_token) if params[:confirmation_token].present?
    redirect_to home_path, :alert => "Your account has been confirmed already." if resource.nil? or resource.confirmed?
  end

  def confirm
    self.resource = resource_class.find_by_confirmation_token(params[resource_name][:confirmation_token])

    if self.resource and self.resource.errors.empty? and user_password(resource) and resource.password_valid? and resource.confirm!
      # if self.resource and self.resource.has_role?(:poast_marketer) and user_password(resource) and resource.password_valid? and resource.confirm!
      set_flash_message :notice, :confirmed
      sign_out(self.resource)
      flash[:notice] = "Your account has been successfully confirmed."
      #redirect_to home_path
      redirect_to itunes_path
    else
      self.resource ||= resource_class.new
      if self.resource.errors.empty?
        self.resource.confirmation_token = params[resource_name][:confirmation_token]
        self.resource.errors.add(:confirmation_token,:not_found)
      end
      render :action => "show"
    end
  end


  private
  def user_password(resource)
    resource.password,resource.password_confirmation = params[:user][:password],params[:user][:password_confirmation]
  end
end