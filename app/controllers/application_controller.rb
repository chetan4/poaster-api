class ApplicationController < ActionController::Base
  protect_from_forgery
  layout :layout_by_resource
  before_filter :authenticate_user!
  helper_method :commandpoast_login


  protected

  def layout_by_resource
    if devise_controller?
      'site'
    else
      'application'
    end
  end


  def commandpoast_login
    ApiConfig.commandpoast_login
  end  
end
