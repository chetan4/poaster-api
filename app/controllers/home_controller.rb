class HomeController < ApplicationController
  skip_before_filter :authenticate_user! , only: [:itunes,:privacy_policy,:service_policy,:support,:marketing,:user_support]
  layout 'site',only: [:itunes,:privacy_policy,:service_policy,:support,:marketing,:user_support]
  def index
  end

  def itunes
  end

  def tracker
  end

  def privacy_policy
  end

  def service_policy
  end

  def support
  end

  def marketing
  end

  def user_support
  end
end
