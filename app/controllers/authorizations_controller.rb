class AuthorizationsController < ApplicationController
  def create
    user_auth = Authorization.from_omniauth(omniauth_token,current_user.id)
    redirect_to home_path
  end

  def share
    params[:user_id] = current_user.id
    params[:company_id] = current_user.company.id
    ## Better way to do the same
    ## SocialProvider::Facebook, ::Twitter , ::Linkedin , ::Emaik
    # threads = []
    ## Make object thread safe to ensure that code dont blow up
    ## because of thread safety issue :)
    shared_responses = ThreadSafe::Array.new
    threads = params[:providers].map do |provider|
      ## We better use Thread because we would be making IO
      ## operation inside
      Thread.new do
        klass = "SocialProvider::#{provider.capitalize}".constantize
        shared_responses << klass.share(params) do |provider|
          provider.track
        end
      end
    end
    threads.map(&:join)

    # Thread code not tested properly but ideally would work except for the exception which could seriously
    # cause a problem
    # threads = []
    # params[:providers].map do |provider|
    #   threads << Thread.new do
    #     "SocialProvider::#{provider.capitalize}".constantize.share(params) do |provider|
    #       provider.track
    #     end
    #   end
    # end
    # shared_responses = threads.collect(&:join)
    rescue => exception
      shared_responses = {status: :failed, reason: exception.message}
    ensure
      render json: shared_responses
  end

  def verify_social_accounts
    social_providers = Authorization.verify_social_accounts(current_user)
    render json: social_providers
  end


  def failure
    flash[:notice] = "Could not authorize to #{params[:strategy].capitalize} currently please try again"
    redirect_to home_path
  end

  private
  def omniauth_token
    request.env["omniauth.auth"]
  end
end