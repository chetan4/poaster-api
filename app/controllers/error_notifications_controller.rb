class ErrorNotificationsController < ApplicationController
  skip_before_filter  :authenticate_user!
  before_filter :authorize_request!

  def notify
    ## Send mail to the developer
    PoasterMailer.error_mails(errors).deliver!
    return render status: :ok
  end  


  def authorize_request!
    unless digest == request.headers['X-Signature']
      return render status: :forbidden
    end  
  end


  def digest
    Digest::SHA1.hexdigest string
  end
    
  def string
    #sha1(yourSecret + error.message + error.page);
    #https://errorception.com/api/webhook#validation
    ApiConfig.signature+error[:message]+error[:page]
  end  
end  