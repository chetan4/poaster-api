class Shareative
  include Mongoid::Document
  include Mongoid::Timestamps
  field :position,type: Integer
  field :shareative_url,type: String
  field :thumb_shareative_url,type: String
  field :shareative_type,type: String
  field :video_url,type: String
  field :name,type: String
  field :description,type: String
  field :company_id,type: Integer
  field :active,type: Boolean, :default => false
  field :side2_type,type: String
  field :side2_text_data,type: String
  field :flip_shareative_url,type: String
  field :thumb_flip_shareative_url
  field :thumb_side2_image_data_url,type: String
  field :side2_link_text,type: String
  field :side2_link_url,type: String

  belongs_to :facade
  #default_scope where(active: true).asc(:position).scoped
 # mount_uploader :shareative,ShareativeUploader
 # mount_uploader :side2_image_data,ShareativeUploader
  scope :active,where(active: true).asc(:position)
  has_many :shareative_share_stats
  #scope :active , where(active: true)
  # default_scope where(active: true)
  #     'shareatives' => [
  #       {
  #         'id' => 'SA0001',
  #         'name' => 'Shareative1',
  #         'shareative_type' => 'Video',
  #         'active' => true,
  #         'facade_id' => 'F0001',
  #         'shareative' => {
  #           'shareative_url' => 'https://google.com',
  #           'thumb_shareative_url' => 'https://thumb.google.com',
  #           'side2' => {
  #             'type' => 'text',
  #             'data' => {
  #               'side2_text_data' => 'Side2Text0001',
  #               'side2_link_url' => 'Side2Link0001',
  #               'side2_link_text' => 'Side2LinkText0001',
  #             }
  #           }
  #         }
  #     },
  #    ...
  #    ]
  def as_json
    {
      id: _id.to_s,
      name: name,
      position: position.to_i,
      shareative_type: shareative_type,
      active: active,
      facade_id: facade_id,
      shareative: shareative_json,
      slug_name: slug_name
    }
  end

  def thumb_url
    if shareative_type == 'image'
      thumb_shareative_url
    else
      YoutubeURL.new(video_url).thumb_url
    end
  end

  def slug_name
    self.name.downcase.gsub(" ", "-").gsub(".","-")
  end

  def video_id
    YoutubeURL.new(video_url).video_id
  end

  private
    def shareative_json
      shareative_core.merge(side2: side2 )
    end

    def shareative_core
      if shareative_type == 'video' then
        YoutubeURL.new(video_url).as_json
      else
        {
          thumb: shareative_url,#thumb_shareative_url,
          url: shareative_url
        }
        #shareative.as_json
      end
    end

    def side2
      if shareative_type == 'image' then
        {
          type: side2_type,
          data: side2_data,
        }
      end
    end

    def side2_data
      if side2_type == 'image' then
        #side2_image_data.as_json
        {
          thumb: flip_shareative_url,#thumb_flip_shareative_url,
          url: flip_shareative_url
        }
      else
        { side2_text_data: CGI.escapeHTML(side2_text_data.to_s), side2_link_text: CGI.escapeHTML(side2_link_text.to_s),side2_link_url: CGI.escapeHTML(side2_link_url.to_s) }
      end
    end
end
