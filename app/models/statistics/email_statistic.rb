class EmailStatistic
  include Mongoid::Document
  include Mongoid::Timestamps
  field :user_id,type: Integer
  field :share_id,type: String
  field :company_id,type: Integer
  
  include UsersStatistic
  
  private
  def column
    :email_share_count
  end
end