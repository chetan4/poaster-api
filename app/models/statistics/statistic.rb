class Statistic
  include Mongoid::Document
  include Mongoid::Timestamps
  field :facebook_share_count,type: Integer,default: 0
  field :twitter_share_count,type: Integer,default: 0
  field :linkedin_share_count,type: Integer,default: 0
  field :gplus_share_count,type: Integer,default: 0
  field :email_share_count,type: Integer,default: 0
  field :total_share_count,type: Integer,default: 0
  field :user_id,type: Integer
  field :company_id,type: Integer
end