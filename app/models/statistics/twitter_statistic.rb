class TwitterStatistic
  include Mongoid::Document
  include Mongoid::Timestamps
  field :user_id,type: Integer
  field :share_id,type: String
  field :company_id,type: Integer
  field :last_updated_at,type: DateTime
  field :retweets,type: Integer,default: 0
  
  include UsersStatistic
 
  private
  def column
    :twitter_share_count
  end
end  
