class FacebookStatistic
  include Mongoid::Document
  include Mongoid::Timestamps
  field :user_id,type: Integer
  field :share_id,type: String
  field :company_id,type: Integer
  field :last_updated_at,type: DateTime
  field :likes,type: Integer,default: 0
  field :comments,type: Integer,default: 0
  
  include UsersStatistic

  private
  def column
    :facebook_share_count
  end
end
