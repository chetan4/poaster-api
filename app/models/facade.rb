class Facade
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name,type: String
  field :position, type: Integer
  field :company_id,type: Integer
  field :poaster_instance_id,type: Integer
  default_scope asc(:position)
  scope :relevant_facades,lambda { |company_id,poaster_instance_id| where(company_id: company_id).where(poaster_instance_id: poaster_instance_id)}
  # scope :for_poaster_instance ,lambda{ |poaster_instance| where(poaster_instance_id: poaster_instance_id)}
  has_many :shareatives

  def as_json
    {
      id: _id.to_s,
      name: name,
      position: position.to_i,
      poaster_instance_id: poaster_instance_id.to_s,
      company_id: company_id.to_s,
      shareatives: shareatives.active.as_json
    }
  end
  # 'facades' => [
  #   {
  #     'id' => 'F00001','name' => 'Facade1','position' => 0,'poaster_instance_id'=> 'PI0001','company_id' => 'C0001',
  #     'shareatives' => [
  #       {
  #         'id' => 'SA0001',
  #         'name' => 'Shareative1',
  #         'shareative_type' => 'Video',
  #         'active' => true,
  #         'facade_id' => 'F0001',
  #         'shareative' => {
  #           'shareative_url' => 'https://google.com',
  #           'thumb_shareative_url' => 'https://thumb.google.com',
  #           'side2' => {
  #             'type' => 'text',
  #             'data' => {
  #               'side2_text_data' => 'Side2Text0001',
  #               'side2_link_url' => 'Side2Link0001',
  #               'side2_link_text' => 'Side2LinkText0001',
  #             }
  #           }
  #         }
  #     },
  #    ...
  #    ]
  # },
  # ...
  #]
end
