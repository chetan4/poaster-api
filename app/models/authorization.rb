class Authorization
  include Mongoid::Document
  include Mongoid::Timestamps
  field :provider,type: String
  field :oauth_token,type: String
  field :oauth_secret,type: String
  field :oauth_refresh_token,type: String
  field :oauth_expires_at,type: DateTime
  field :user_id,type: Integer
  field :name,type: String
  field :uid,type: String
  
  
  PROVIDER_TYPE = ["twitter","facebook","linkedin"]

  validates_uniqueness_of :provider, scope: :user_id
  PROVIDER_TYPE.each do |provider_type|
    define_method("#{provider_type}?") { provider == provider_type}
  end

  def oauth_token_valid?
    true
    # if self.oauth_expires_at == nil
    #   true
    # else
    #   true
    # end
  end

  class << self
    def from_omniauth(auth,user_id)
      where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.oauth_token = auth.credentials.token
        user.oauth_secret = auth.credentials.secret
        user.oauth_refresh_token = refresh_token(auth)
        user.oauth_expires_at = expires_at_time(auth)
        user.user_id = user_id
        user.save!
      end
    end

    def refresh_token(auth)
      case auth.provider
      when 'google_oauth2'
        auth.credentials.refresh_token
      else
        nil
      end
    end

    def expires_at_time(auth)
      case auth.provider
        when 'facebook'
          Time.at(auth.credentials.expires_at)
        when 'twitter'
          ## Twitter donot expires the access token
          nil
        when 'linkedin'
          Time.now.advance(:seconds => auth.extra.access_token.params[:oauth_authorization_expires_in].to_i) 
        when 'google_oauth2'
          Time.at(auth.credentials.expires_at)
        else
          nil
      end
      rescue nil
    end  

    def verify_social_accounts(user)
      Authorization.where(user_id: user.id).pluck(:provider).inject({}) {|providers,provider| providers[provider.to_sym] = true ; providers }
    end
  end
end