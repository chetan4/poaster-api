class User < ActiveRecord::Base
  devise :database_authenticatable,:recoverable, :rememberable, :trackable, :validatable,:confirmable

  include RoleModel
  roles_attribute :roles_mask
  roles :poast_master, :curator, :poast_marketer, :super_admin

  belongs_to :company
  has_many :access_controls ,dependent: :destroy
  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :first_name, :last_name, :department,:roles, :company_attributes,:profile
  # has_many :poaster_instances,through: :access_controls ,dependent: :destroy
  scope :active , where(active: true)
  mount_uploader :profile,ProfileUploader

  def as_json
    {
      id: id.to_s,
      name: full_name,
      email: email,
      company: company.as_json,
      default_poaster_instance: default_poaster_instance.as_json,
      poaster_instances: poaster_instances.as_json
    }
  end

  def validate_password?(password)
    unless valid_password?(password)
      self.errors.add(:password,'current password is not valid')
    end
    return errors.empty?
  end

  def password_valid?
    self.errors[:password] = 'must be present' if self.password.blank?
  
    if self.password and self.password != self.password_confirmation
      self.errors[:password_confirmation] = 'does not match'
    end
    self.errors.empty?
  end

  def statistics
    {
      facebook: facebook_statistic.count,
      twitter: twitter_statistic.count,
      linkedin: linkedin_statistic.count,
      email: email_statistic.count,
      poast: total_statistic,
      measurable: total_measurable
    }
  end

  def full_name
    [first_name,last_name].join(' ')
  end

  def poaster_instances
    company.poaster_instances
  end
 
  def build_poaster_json(poaster_instance_id="0")
    find_poaster_instance(poaster_instance_id).as_json.merge facades: facade_json
  end

  def poaster_instance_schema
    find_poaster_instance.as_json.merge facades: facade_json
  end

  def facade_json
    Facade.relevant_facades(company.id,find_poaster_instance.id).as_json
  end

  def find_poaster_instance(poaster_instance_id="0")
    ## 0 implies no match for the enumerator and take the default poaster instances
    (poaster_instances.where(:id => id).first || default_poaster_instance)
  end

  def default_poaster_instance
    poaster_instances.first
  end

  def welcome_email
    UserMailer.welcome_email(self).deliver
  end

  private
  
  def facebook_statistic
    @facebook_statistic ||= FacebookStatistic.where(user_id: id,company_id: company.id)
  end

  def twitter_statistic
    @twitter_statistic ||= TwitterStatistic.where(user_id: id,company_id: company.id)
  end

  def linkedin_statistic
    @linkedin_statistic ||= LinkedinStatistic.where(user_id: id,company_id: company.id)
  end

  def email_statistic
    @email_statistic ||= EmailStatistic.where(user_id: id,company_id: company.id)
  end

  def total_statistic
    facebook_statistic.count + twitter_statistic.count  + linkedin_statistic.count + email_statistic.count
  end
  
  def poast_statistic
    [facebook_statistic.count-1,0].max + [twitter_statistic.count-1,0].max + [linkedin_statistic.count-1,0].max + [email_statistic.count-1,0].max
  end

  def total_measurable
    @measurable_statistic ||= (first_poasting_measurable + poasting_measurable + facebook_measurable + twitter_measurable + linkedin_measurable + email_measurable)
  end

  def first_poasting_statistic
    [1,facebook_statistic.count].min + [1,twitter_statistic.count].min + [1,linkedin_statistic.count].min + [1,email_statistic.count].min
  end
  
  def first_poasting_measurable
    @first_poasting_measurable ||= first_poasting_statistic * 10
  end
  
  def poasting_measurable
    @poasting_measurable ||= poast_statistic * 25
  end
  
  def facebook_measurable
    @facebook_measurable ||= [facebook_statistic.sum('likes') , facebook_statistic.sum('comments')].sum * 50
  end
  
  def twitter_measurable
    @twitter_measurable ||= twitter_statistic.sum('retweets') * 50
  end
  
  def linkedin_measurable
    @linkedin_measurable ||= [linkedin_statistic.sum('likes'),linkedin_statistic.sum('comments')].sum * 50
  end
  
  def email_measurable
    0
  end
  
  def total_company_statistics
    FacebookStatistic.statistics(company_id).count + TwitterStatistic.statistics(company_id).count + LinkedinStatistic.statistics(company_id).count + EmailStatistic.statistics(company_id).count
  end

  def total_employees
    User.where(company_id: company_id).count
  end  
end
