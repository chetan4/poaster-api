class Company < ActiveRecord::Base
  attr_accessible :name, :website
  has_many :poaster_instances

  def as_json
    {
      name: self.name,
      website: self.website,
      id: self.id.to_s
    }
  end
end
