class AccessControl < ActiveRecord::Base
  attr_accessible :company_id, :poaster_instance_id, :user_id,:view,:manage
  belongs_to :user
  belongs_to :poaster_instance

  def as_json
    {
      read: view,
      write: manage
    }
  end  
end