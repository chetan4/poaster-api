class PoasterInstance < ActiveRecord::Base
  attr_accessible :company_id, :name
  belongs_to :company
  has_many :access_controls ,dependent: :destroy
  has_many :users,through: :access_controls ,dependent: :destroy

  def as_json
    {
      id: id.to_s,
      name: name,
      company_id: company_id.to_s,
      access_controls: my_access_controls
    }
  end 


  def my_access_controls
    !access_controls.present? ? { read: true,write: true} : access_controls.as_json
  end 
end