module ApplicationHelper

  def error_class(resource,field)
    resource.errors[field].present? ? 'error_class' : ''
  end

	def itunes_url
		'https://itunes.apple.com/us/app/poaster/id895711397?mt=8'
	end
end
