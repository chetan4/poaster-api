class PoasterMailer < ActionMailer::Base
  default from: "from@example.com"

  def share(options)
    to = options[:to]
    from = options[:from]
    cc = options[:cc]
    bcc = options[:bcc]
    subject = options[:subject]
    @body = options[:body]
    @share_id = options[:share_id]
    mail(to: to,from: from,cc: cc,bcc: bcc,subject: subject)
  end


  def error_mails(options)
    from = ApiConfig.sender_email_address
    to = ApiConfig.receiver_email_address
    subject = " Error Occurred on  #{options[:page]} "
    @body = options[:message]
    @page = options[:page] 
    mail(to: to,from: from,cc: cc,bcc: bcc,subject: subject)
  end  
end
