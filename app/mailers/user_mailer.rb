class UserMailer < ActionMailer::Base
  default from: "no-reply@poaster.me"

  def welcome_email(user)
    @user = user
    to = user.email
    subject = 'Poaster Command Poast Account Confirmation'
    mail(to: to,subject: subject)
  end
end    
