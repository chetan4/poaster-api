# encoding: utf-8

class ProfileUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  include CarrierWave::MagickHelper
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  
  def cache_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/cache_dir"
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end


  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    # For Rails 3.1+ asset pipeline compatibility:
    # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  
    "/images/fallback/" + [version_name, "default.jpg"].compact.join('_')
  end

  def auto_orient
    manipulate! do |img|
      img = img.auto_orient
    end
  end

  process :auto_orient
  # Process files as they are uploaded:
  #process :resize_to_fit => [125,100]
  #process :resize_to_limit => [125,100]
  version :forty_two do
    process :resize_to_geometry => [42,42]
  end
  
  version :thirty do
    process :resize_to_geometry  => [30,30]
  end    
  # version :thumb do
  #   process :resize_to_geometry => [125,100]
  # end
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :scale => [100, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
