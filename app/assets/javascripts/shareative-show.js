var flipAngle = 0;
var flip_transform = function(flipAngle) {
    var $flipper = $("#flipper");
    $flipper.css({
        '-webkitTransform': 'rotateY(' + flipAngle +'deg)'
    });
};
var flip_full = function() {
    flipAngle=360;
    flip_transform(flipAngle);
};
var flip_right = function() {
    flipAngle+=180;
    flip_transform(flipAngle);
};
var flip_left = function(event) {
    flipAngle-=180;
    flip_transform(flipAngle);
};
var setup_flip = function(){
    $("#flipper_container").on("swiperight", flip_right);
    $("#flipper_container").on("swipeleft", flip_left);
    flip_full();
}


$(function(){
    var videoBannerWidth = $('#video_banner').width(),
        videoBannerHeight =  $('#video_banner').height(),
        watermark = '<div id="client_brand_watermark_wrapper"><img id="client_brand_watermark" src="https://poasterappsocial.s3.amazonaws.com/poaster/logo.png"></div>';

    $('#ytplayer').attr({
        width: videoBannerWidth,
        height: videoBannerHeight
    });

    // Set up flip
    setup_flip();

    $('body#onlyShareative').append(watermark);
  });