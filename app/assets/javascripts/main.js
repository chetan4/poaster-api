// UI Transitions
function poasted (callback) {
    $('#sharing_confirmation_message').css({ zIndex: 999});
    $('#sharing_confirmation_message').animate({
        opacity: 1
    }, 400);

    setTimeout(function(){
        $('#sharing_confirmation_message').animate({
            opacity: 0
        }, 400);
        setTimeout(function(){
            $('#sharing_confirmation_message').css({ zIndex: -1});
            $('#sharing_menu_cancel_btn').click();
        }, 1200);
    }, 1500);
    if ( callback && typeof callback === 'function') {
        callback();
    }
}
// Total no. of poasts by the user
function Poasts(totalElement, averageElement) {
    $.ajax({
        url: '/users/statistics.json',
        method: 'GET',
    }).done(function(response) {
        $(totalElement).html(response.poast);
        $(averageElement).html(response.measurable);
        if ( response.total > 1 || response.average > 1) {
            $(totalElement).siblings('h3').text('Poasts');
            $(averageElement).siblings('h3').text('Credits');
        }
    });
}

// Total no. of poasts by the user
function averagePoasts(element) {
    $.ajax({
        url: '/users/statistics.json',
        method: 'GET',
    }).done(function(response) {
        $(element).html(response.average);
    });
}

$(function(){

    var windowHeight     = $(window).height(),
        windowWidth      = $(window).width(),
        // footerHeight     = $('#footer').height(),
        statusBarHeight = $('#status-bar').height(),
        // headerHeight    = $('#header').height() + statusBarHeight,
        headerHeight    = $('#header').height(),
        // globalObjHeight = windowHeight - ( headerHeight + footerHeight ),
        globalObjHeight = windowHeight - headerHeight,
        profileStasWidth = windowWidth - 100,
        profilePicButton = ( headerHeight / 100 ) * 50,
        profilePicButtonTop = ( headerHeight / 100 ) * 25,
        profilePicButtonRight = ( headerHeight / 100 ) * 15,
        profileStasItemsWidth = profileStasWidth - profileStasItemsGutter,
        profileStasItemsGutter = (( profileStasWidth / 3 ) / 100 ) * 5;
        
        // Listen for orientation changes
        $(window).on('orientationchange',function(e) {
            if ( window.orientation === 90 ) {
                window.alert('Poaster experience may not be optimum in landscape');
            }
        });

        $('#profile-view, #change-password-view').css({ height: globalObjHeight });

        $('#profile-btn').fastClick(function(){

            $(this).toggleClass('active');
            if( $(this).hasClass('active') ) {
                $('#profile-view').css({
                    'transform': 'translate3d(0px, '+ headerHeight +'px, 0px)'
                });
                verify_social_accounts($('#profile_accounts'))
            } else {
                $('#profile-view').css({
                    'transform': 'translate3d(0px, -'+ globalObjHeight +'px, 0px)'
                });
            }

        });

        $('#change_password_btn').fastClick(function(){
            document.querySelector('#authentication form').reset();
            $(this).toggleClass('active');
            if( $(this).hasClass('active') ) {
                // Pull up the profile view
                $('#profile-view').css({
                    '-webkitTransform': 'translate3d(0px, -'+ ( globalObjHeight ) +'px, 0px)'
                });
                $('#change-password-view').css({
                    '-webkitTransform': 'translate3d(0px, '+ headerHeight +'px, 0px)'
                });
            } else {
                $('#change-password-view').css({
                    '-webkitTransform': 'translate3d(0px, -'+ ( globalObjHeight ) +'px, 0px)'
                });
            }
        });

        $('#cancel-change-password').fastClick(function(event){
            event.preventDefault();
            $('#change_password_btn, #profile-btn').removeClass('active');
            $('#change-password-view').css({
                '-webkitTransform': 'translate3d(0px, -'+ ( globalObjHeight ) +'px, 0px)'
            });
            $('#profile-btn').addClass('active');
            $('#profile-view').css({
                '-webkitTransform': 'translate3d(0px, '+ headerHeight +'px, 0px)'
            });
        });

        $('#change-password-view form').submit(function(event){
            event.preventDefault();
            var oldPasswordVal     = $('#password_options_old_password').val(),
                newPasswordVal     = $('#password_options_password').val(),
                confirmPasswordVal = $('#password_options_password_confirmation').val(),
                dataObj = 'password_options[old_password]='+ oldPasswordVal +'&password_options[password]='+ newPasswordVal +'&password_options[password_confirmation]='+ confirmPasswordVal;

            // console.log(dataObj);

            $.ajax({
                url: '/users/password/create.json',
                method: 'POST',
                data: dataObj,
            }).done(function(response) {
                console.log(response);
                $('#changePasswordNotice').html(response.message);
                if ( response.status === 'success' ) {
                    setTimeout(function(){
                        $('#cancel-change-password').click();
                        $('#changePasswordNotice').hide();
                    }, 2000);
                }
            });
        });
        // Poasts('#totalPoasts h2', '#averagePoasts h2');
});
