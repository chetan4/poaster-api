
function build_poaster() {
    
    var windowWidth            = $(window).width(),
        windowHeight           = $(window).height(),
        headerHeight           = $('#header').height(),
        horizontalGutter       = windowWidth / 100 * 20, // Leaving 20% padding horizontally
        verticalGutter         = windowHeight / 100 * 20, // Leaving 20% padding vertically
        size                   = windowWidth,
        sizeHeight             = windowHeight,
        globalObjHeight        = windowHeight - headerHeight,
        facadePadding          = size/100*3,
        availableFacadeWidth   = size - ( (size/100*3) * 2 ),
        // availableFacadeHeight  = sizeHeight - ( (sizeHeight/100*3) * 2 ),
        availableFacadeHeight  = sizeHeight,
        shareativeGutter       = availableFacadeWidth / 100 * 6,
        shareativeBottomMargin = availableFacadeHeight / 70 * 5,
        // shareativeWidth        = (availableFacadeWidth / 2) - shareativeGutter,
        shareativeWidth        = size, 
        facadeHeight           = globalObjHeight - 250, // 60
        // facadeHeight           = globalObjHeight - 150,
        // shareativeHeight       = (facadeHeight - 110) / 3,
        shareativeHeight       = shareativeWidth/1.32;
        $profileAccounts = $('#profile_accounts');
        
        // Execution
        // alert(shareativeHeight);
        $('#poaster-cube-container').css({ 
            width: size+'px', 
            height: globalObjHeight+'px',
            'perspective':size*2
        });
        $('#poaster-cube').css({ 
            'transform-origin':size+'px '+size+'px',
            'transform':'translateX(-'+size/2+'px) translateY('+headerHeight+'px) translateZ(-'+ size/2 + 'px)'
        });
        $('#poaster-cube .facade').css({ 
            width: size, 
            height: globalObjHeight 
        });

        for ( i = 0; i < 4; i++ ) {
            $('#poaster-cube .facade:nth-child(' + (i+1) +')').css({
                'transform': 'rotateY(' + (i * 90) + 'deg) translateZ('+ size/2 + 'px)',
                'left':size/2+'px'
            });
            // console.log('Angle ', i * (360 / facadesLength));
        }
        // console.log('Apothem ', apothem);
        
        // $('#poaster-cube').css({
        //     '-webkitPerspective': perspective_factor
        // });

        $('.shareative').css({
            width: shareativeWidth,
            height: shareativeHeight,
        });
        
        $('#sharing_confirmation_message').css({ height: windowHeight });



        $('#shareative_view, #shareative_content, #sharing_view').css({
            width: windowWidth,
            height: windowHeight
        });

        verify_social_accounts($profileAccounts)
        // Informative authenticated Ajax service providers

        
        var deg = 0; // Setting a degree first
        var angle = 90;
        // Add listeners

        $('#poaster-cube-container').on("swiperight", function(event){
            event.preventDefault();

            deg = deg + angle; // Incrementing with passed angle because right swipe
            console.log(deg);
            $('#poaster-cube').css({
              'transform': 'translateX(-'+size/2+'px) translateY('+headerHeight+'px) translateZ(-'+ size/2 + 'px) rotateY(' + deg + 'deg)'
            });
        });

        $('#poaster-cube-container').on("swipeleft", function(event){
            event.preventDefault();

            deg = deg - angle; // Decrementing with passed angle because left swipe
            console.log(deg);
            $('#poaster-cube').css({
              'transform': 'translateX(-'+size/2+'px) translateY('+headerHeight+'px) translateZ(-'+ size/2 + 'px) rotateY(' + deg + 'deg)'
            });
        });

}


function verify_social_accounts(profileAccounts) {
  $.ajax({
        url: '/verify_social_accounts',
        method: 'GET',
    }).done(function(data) {
        console.log(data);
        // var networksLength = Object.keys(data).length;
        // if ( networksLength > 0 && networksLength < 3 ) {
        //     $('#sharing_view').prepend('<div class="notice">You can share on more networks by authenticating yourself from the profile view</div>');
        // }
        profileAccounts.children('.active').removeClass('active');
        if (data.facebook) {
            profileAccounts.children('#facebook').addClass('active');
        }
        if (data.twitter) {
            profileAccounts.children('#twitter').addClass('active');
        }
        if (data.linkedin) {
            profileAccounts.children('#linkedin').addClass('active');
        }
    });
}